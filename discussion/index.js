// Express Setup 

// npm install nodemon
// npm init
// npm install express
// npm install mongoose

const express = require("express")


const mongoose = require("mongoose");

const app = express();
const port = 3001;

//[section] - Mongoose Connection
//Mongoose uses the 'connect' function to connect to the cluster in our MongoDb Atlas.

/*

It takes 2 arguments:
1. Connection string from our MongoDB Atlas.
2. Object contains the middlewares/standards.

*/

mongoose.connect(`mongodb+srv://jessielibita:admin123@zuittbatch197cluster.gq4lrrn.mongodb.net/S35-Activity?retryWrites=true&w=majority`,{
	// {newUrlParser: true} - it allows us to avoid any current and/or future error while connecting to MongoDB
	useNewUrlParser: true,
	// useUnifiedTopology if "false" by default. Set to true to opt in to using the MongoDb driver's new connection management engine
	useUnifiedTopology: true
})

// Initializes the mongoose connection to the MongoDb Db by assigning 'mongoose.connection' to the 'db' variable.
let db = mongoose.connection

// Lister to the events of the connection by using the 'on()' function of the mongoose connection and logs the details in the console based of the event (error or successful)
db.on('error', console.error.bind(console, "Connection"));
db.once('open', () => console.log('Connection to MongoDB!'))


// Creating a schema
const taskSchema = new mongoose.Schema({
	//Define the fields with their corrsponding data types
	//For a task, it needs a "task name" and its data type will be "String" and the default value is "pending"
	name: String,
	status: {
		type: String,
		default: 'Pending'
	}
})

//Models
//The variable/object "Task" can now be used to run commands
const Task = mongoose.model('Task', taskSchema);

//CREATING THE ROUTES

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));


//Create a user route
app.post('/tasks', (req, res) => {
	// business logic
	Task.findOne({name: req.body.name}, (error, result) => {
		if(error){
			return res.send(error)
		} else if (result != null && result.name == req.body.name) {
			return res.send('Duplicate task found')
		} else {
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save((error, savedTask) => {
				if(error) {
					return console.error(error)
				} else {
					return res.status(201).send('New Task Created!')
				}
			})
		}
	})
})

// Get all users from collection - get all users route
app.get('/tasks',(req, res) =>{
	Task.find({}, (error, result) =>{
		if(error){
			return res.send(error)
		}else{
			return res.status(200).json({
				tasks: result
			})
		}
	})
})


// Creating a schema
const userSchema = new mongoose.Schema({
	//Define the fields with their corrsponding data types
	//For a task, it needs a "task name" and its data type will be "String" and the default value is "pending"
	userName: String,
	status: {
		type: String,
		default: 'Pending'
	}
})

//Models
//The variable/object "Task" can now be used to run commands
const User = mongoose.model('User', userSchema);

//Register a user route
app.post('/signup', (req, res) => {
	// business logic
	User.findOne({userName: req.body.userName}, (error, result) => {
		if(error){
			return res.send(error)
		} else if (result != null && result.userName == req.body.userName) {
			return res.send('Username is already taken')
		} else {
			let newUser = new User({
				userName: req.body.userName
			})

			newUser.save((error, savedTask) => {
				if(error) {
					return console.error(error)
				} else {
					return res.status(201).send('New User Registered!')
				}
			})
		}
	})
})

// Get all users from collection - get all users route
app.get('/signup',(req, res) =>{
	User.find({}, (error, result) =>{
		if(error){
			return res.send(error)
		}else{
			return res.status(200).json({
				User: result
			})
		}
	})
})




app.listen(port, () => console.log(`Server is running at port: ${port}`))
